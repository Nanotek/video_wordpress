<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Mission local</title>
    <?php wp_head(); ?>
</head>

<?php
    if (is_front_page() ):
        $mission_classes = array( 'mission-class', 'my-class' );
    else:
        $mission_classes = array( 'no-mission-class');
    endif;

?>

<body <?php body_class($mission_classes); ?>>

    <div class="container">

        <div class="row">

            <div class="col-xs-12">
            
                <nav class"nav navbar-default">
                    <div class="container-fluid">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-exemple-navbar-collapse-1">Ouvrir menu
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="#">Mission Theme</a>
                        </div>
                        <div class="collapse navbar-collapse" id="bs-exemple-navbar-collapse-1">
                            <?php 
                                wp_nav_menu(array(
                                    'theme_location' => 'primary',
                                    'container' => false,
                                    'menu_class' => 'nav navbar-nav navbar-right'
                                    )
                                );
                            ?>
                        </div>
                    </div><!-- /.container-fluid -->
                </nav>

            </div>
            
        </div>
    </div>

    <?php wp_nav_menu(array('theme_location'=>'primary')); 
    
        var_dump('get_custom_header');
    
    ?>

    <img src="<?php header_image(); ?>" height="<?php echo get_custom_header()->height; ?>" width="<?php echo get_custom_header()->width; ?>">